package com.mmall.common;

import com.mmall.exception.PermissionException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @Auther 19135
 * @Date 2018/12/30 16:07
 * @Description 全局异常处理bean
 */
@Slf4j
public class SpringExceptionResolver implements HandlerExceptionResolver {
	public ModelAndView resolveException(HttpServletRequest request, HttpServletResponse response, Object o, Exception e) {
		//这个有个判断，需要判断该异常出现时，应该返回是页面还是一个json数据
		//有两种方式，一种是从request的请求相应格式判断比较复杂
		//这里使用第二种。
		//所有的url  必须以json结尾 或者 page结尾

		String url = request.getRequestURL().toString();
		ModelAndView mv;
		String defaultMsg = "Systime error";

		//要求项目中所有请求必须使用json结尾，page必须使用page结尾
		if (url.endsWith(".json")){
			if (e instanceof PermissionException){
				JsonData result = JsonData.fail(e.getMessage());
				//之所以是jsonView是因为在spring中配置了json数据的name值
				mv = new ModelAndView("jsonView",result.toMap());
			}else {
				log.error("unknow json exception,url:"+url,e);
				JsonData result = JsonData.fail(defaultMsg);
				mv = new ModelAndView("jsonView",result.toMap());
			}
		}else if (url.endsWith(".page")){
			log.error("unknow page exception,url:"+url,e);
			JsonData result = JsonData.fail(defaultMsg);
			//此时会跳转到一个页面
			mv = new ModelAndView("exception",result.toMap());
		}else{
			log.error("unknow exception,url:"+url,e);
			JsonData result = JsonData.fail(defaultMsg);
			mv = new ModelAndView("jsonView",result.toMap());
		}
		return mv;
	}
}
