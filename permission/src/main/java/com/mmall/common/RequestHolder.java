package com.mmall.common;

import com.mmall.model.SysUser;

import javax.servlet.http.HttpServletRequest;

/**
 * @Auther 19135
 * @Date 2019/3/21 20:24
 * @Description ${用来存放每次请求的request对象方便程序访问}$
 */
public class RequestHolder {
	private static final ThreadLocal<SysUser> userHolder =new ThreadLocal<>();

	private static final ThreadLocal<HttpServletRequest>  requestHolder = new ThreadLocal<>();

	public static void add(SysUser sysUser){
		userHolder.set(sysUser);
	}

	public static void add(HttpServletRequest httpServletRequest){
		requestHolder.set(httpServletRequest);
	}
	public static SysUser getCurrentUser() {
		return userHolder.get();
	}

	public static HttpServletRequest getCurrentRequest() {
		return requestHolder.get();
	}

	public static void remove() {
		userHolder.remove();
		requestHolder.remove();
	}
}
