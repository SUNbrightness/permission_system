package com.mmall.common;

import lombok.Data;

import java.util.HashMap;
import java.util.Map;

/**
 * @Auther 19135
 * @Date 2018/12/30 15:56
 * @Description $end$
 */
@Data
public class JsonData {

	private boolean ret;

	private  String msg;

	private Object data;
	public JsonData(boolean ret) {
		this.ret = ret;
	}

	public static JsonData success(Object object, String msg) {
		JsonData jsonData = new JsonData(true);
		jsonData.data = object;
		jsonData.msg = msg;
		return jsonData;
	}

	public static JsonData success(Object object) {
		JsonData jsonData = new JsonData(true);
		jsonData.data = object;
		return jsonData;
	}

	public static JsonData success() {
		return new JsonData(true);
	}

	public static JsonData fail(String msg) {
		JsonData jsonData = new JsonData(false);
		jsonData.msg = msg;
		return jsonData;
	}

	public Map<String, Object> toMap() {
		HashMap<String, Object> result = new HashMap<String, Object>();
		result.put("ret", ret);
		result.put("msg", msg);
		result.put("data", data);
		return result;
	}
}
