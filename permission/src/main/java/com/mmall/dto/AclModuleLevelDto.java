package com.mmall.dto;

import com.google.common.collect.Lists;
import com.mmall.model.SysAclModule;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.beans.BeanUtils;

import java.util.List;

/**
 * @Auther 19135
 * @Date 2019/3/23 21:03
 * @Description ${权限模块构成的树形图$
 */
@Getter
@Setter
@ToString
public class AclModuleLevelDto extends SysAclModule {

	private List<AclModuleLevelDto> aclModuleList = Lists.newArrayList();

	private List<AclDto> aclList = Lists.newArrayList();

	public static AclModuleLevelDto adapt(SysAclModule aclModule) {
		AclModuleLevelDto dto = new AclModuleLevelDto();
		BeanUtils.copyProperties(aclModule, dto);
		return dto;
	}

}
