package com.mmall.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;

/**
 * @Auther 19135
 * @Date 2019/3/23 21:05
 * @Description ${end}$
 */
@Getter
@Setter
@ToString
public class SearchLogDto {

	private Integer type; // LogType

	private String beforeSeg;

	private String afterSeg;

	private String operator;

	private Date fromTime;//yyyy-MM-dd HH:mm:ss

	private Date toTime; //yyyy-MM-dd HH:mm:ss

}
