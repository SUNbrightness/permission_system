package com.mmall.exception;

/**
 * @Auther 19135
 * @Date 2018/12/30 16:13
 * @Description $end$
 */
public class PermissionException extends  RuntimeException{
	public PermissionException() {
		super();
	}

	public PermissionException(String message) {
		super(message);
	}

	public PermissionException(String message, Throwable cause) {
		super(message, cause);
	}

	public PermissionException(Throwable cause) {
		super(cause);
	}


}
