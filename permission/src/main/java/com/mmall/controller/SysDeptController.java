package com.mmall.controller;

import com.mmall.common.JsonData;
import com.mmall.dto.DeptLevelDto;
import com.mmall.param.DeptParam;
import com.mmall.service.SysDeptService;
import com.mmall.service.SysTreeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * @Auther 19135
 * @Date 2019/1/13 19:21
 * @Description ${end}$
 */
@Controller
@RequestMapping("/sys/dept")
@Slf4j
public class SysDeptController {

	@Autowired
	SysDeptService sysDeptService;

	@Autowired
	SysTreeService sysTreeService;

	@RequestMapping("/dept.page")
	public ModelAndView page() {
		return new ModelAndView("dept");
	}


	@RequestMapping("/save.json")
	@ResponseBody
	public JsonData savaDept(DeptParam param){
		sysDeptService.sava(param);
		return JsonData.success();
	}

	@RequestMapping("/tree.json")
	@ResponseBody
	public JsonData tree(){
		List<DeptLevelDto> deptLevelDtos = sysTreeService.deptTree();
		return JsonData.success(deptLevelDtos);
	}


	@RequestMapping("/update.json")
	@ResponseBody
	public JsonData updateaDept(DeptParam param){
		sysDeptService.update(param);
		return JsonData.success();
	}
	@RequestMapping("/delete.json")
	@ResponseBody
	public JsonData delete(@RequestParam("id") int id) {
		sysDeptService.delete(id);
		return JsonData.success();
	}



}
