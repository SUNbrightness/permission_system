package com.mmall.controller;

import com.google.common.collect.Lists;
import com.mmall.common.JsonData;
import com.mmall.exception.ParamException;
import com.mmall.model.SysAcl;
import com.mmall.util.BeanValidator;
import com.mmall.util.JsonMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/test")
@Slf4j
public class TestController {
	
	@RequestMapping("/hello.json")
	@ResponseBody
	public JsonData hello(SysAcl s)  throws ParamException {
		BeanValidator.check(s);
		String s1 = JsonMapper.obj2String(s);
		System.out.println(s1);
		log.info("hello");
		List list = Lists.newArrayList();
		return JsonData.success("hell");
	}

}
