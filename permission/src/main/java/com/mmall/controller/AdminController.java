package com.mmall.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * @Auther 19135
 * @Date 2019/3/21 20:35
 * @Description ${admin}$
 */
@Controller
@RequestMapping("/admin")
public class AdminController {

	@RequestMapping("index.page")
	public ModelAndView index()
	{
		return new ModelAndView("admin");
	}
}
