package com.mmall.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import redis.clients.jedis.ShardedJedis;
import redis.clients.jedis.ShardedJedisPool;

import javax.annotation.Resource;

/**
 * @Auther 19135
 * @Date 2019/3/24 19:18
 * @Description ${end}$
 */
@Service("redisPool")
@Slf4j
public class RedisPool {

	@Resource(name = "shardedJedisPool")
	private ShardedJedisPool shardedJedisPool;


	public ShardedJedis instance() {
		return shardedJedisPool.getResource();
	}

	public void safeClose(ShardedJedis shardedJedis) {
		try {
			if (shardedJedis != null) {
				shardedJedis.close();
			}
		} catch (Exception e) {
			log.error("return redis resource exception", e);
		}
	}

}
