package com.mmall.service;

import com.google.common.base.Preconditions;
import com.mmall.beans.PageQuery;
import com.mmall.beans.PageResult;
import com.mmall.common.RequestHolder;
import com.mmall.dao.SysAclMapper;
import com.mmall.dao.SysAclModuleMapper;
import com.mmall.exception.ParamException;
import com.mmall.model.SysAcl;
import com.mmall.model.SysAclModule;
import com.mmall.model.SysLog;
import com.mmall.param.AclModuleParam;
import com.mmall.param.AclParam;
import com.mmall.util.BeanValidator;
import com.mmall.util.IpUtil;
import com.mmall.util.LevelUtil;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * @Auther 19135
 * @Date 2019/3/24 19:49
 * @Description ${权限模块}$
 */
@Service
public class SysAclModuleService {

	@Resource
	private SysAclModuleMapper sysAclModuleMapper;

	@Resource
	private SysAclMapper sysAclMapper;

	@Resource
	private SysLogService sysLogService;

	public void save(AclModuleParam param) {
		BeanValidator.check(param);
		if(checkExist(param.getParentId(), param.getName(), param.getId())) {
			throw new ParamException("同一层级下存在相同名称的权限模块");
		}
		SysAclModule aclModule = SysAclModule.builder().name(param.getName()).parentId(param.getParentId()).seq(param.getSeq())
				.status(param.getStatus()).remark(param.getRemark()).build();
		aclModule.setLevel(LevelUtil.calculateLevel(getLevel(param.getParentId()), param.getParentId()));
		aclModule.setOperator(RequestHolder.getCurrentUser().getUsername());
		aclModule.setOperateIp(IpUtil.getRemoteIp(RequestHolder.getCurrentRequest()));
		aclModule.setOperateTime(new Date());
		sysAclModuleMapper.insertSelective(aclModule);
		sysLogService.saveAclModuleLog(null, aclModule);
	}
	public boolean checkExist(int aclModuleId, String name, Integer id) {
		return sysAclMapper.countByNameAndAclModuleId(aclModuleId, name, id) > 0;
	}

	public void update(AclModuleParam param) {
		BeanValidator.check(param);
		if(checkExist(param.getParentId(), param.getName(), param.getId())) {
			throw new ParamException("同一层级下存在相同名称的权限模块");
		}
		SysAclModule before = sysAclModuleMapper.selectByPrimaryKey(param.getId());
		Preconditions.checkNotNull(before, "待更新的权限模块不存在");

		SysAclModule after = SysAclModule.builder().id(param.getId()).name(param.getName()).parentId(param.getParentId()).seq(param.getSeq())
				.status(param.getStatus()).remark(param.getRemark()).build();
		after.setLevel(LevelUtil.calculateLevel(getLevel(param.getParentId()), param.getParentId()));
		after.setOperator(RequestHolder.getCurrentUser().getUsername());
		after.setOperateIp(IpUtil.getRemoteIp(RequestHolder.getCurrentRequest()));
		after.setOperateTime(new Date());

		updateWithChild(before, after);
		sysLogService.saveAclModuleLog(before, after);
	}

	@Transactional
	void updateWithChild(SysAclModule before,SysAclModule after){
		String newLevelPrefix = after.getLevel();
		String oldLevelPrefix = before.getLevel();
		//如果层级发生变化需要更新
		if (!after.getLevel().equals(before.getLevel())){
			//获得该层级所有子元素
			List<SysAclModule> aclModuleList = sysAclModuleMapper.getChildAclModuleListByLevel(before.getLevel());
			if (CollectionUtils.isNotEmpty(aclModuleList)) {
				for (SysAclModule aclModule : aclModuleList) {
					String level = aclModule.getLevel();
					if (level.indexOf(oldLevelPrefix) == 0) {
						level = newLevelPrefix + level.substring(oldLevelPrefix.length());
						aclModule.setLevel(level);
					}
				}
				sysAclModuleMapper.batchUpdateLevel(aclModuleList);
			}
		}
		sysAclModuleMapper.updateByPrimaryKeySelective(after);
	}

	public String generateCode() {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
		return dateFormat.format(new Date()) + "_" + (int)(Math.random() * 100);
	}

	public PageResult<SysAcl> getPageByAclModuleId(int aclModuleId, PageQuery page) {
		BeanValidator.check(page);
		int count = sysAclMapper.countByAclModuleId(aclModuleId);
		if (count > 0) {
			List<SysAcl> aclList = sysAclMapper.getPageByAclModuleId(aclModuleId, page);
			return PageResult.<SysAcl>builder().data(aclList).total(count).build();
		}
		return PageResult.<SysAcl>builder().build();
	}

	private String getLevel(Integer aclModuleId) {
		SysAclModule aclModule = sysAclModuleMapper.selectByPrimaryKey(aclModuleId);
		if (aclModule == null) {
			return null;
		}
		return aclModule.getLevel();
	}


	public void delete(int aclModuleId) {
		SysAclModule aclModule = sysAclModuleMapper.selectByPrimaryKey(aclModuleId);
		Preconditions.checkNotNull(aclModule, "待删除的权限模块不存在，无法删除");
		if(sysAclModuleMapper.countByParentId(aclModule.getId()) > 0) {
			throw new ParamException("当前模块下面有子模块，无法删除");
		}
		if (sysAclMapper.countByAclModuleId(aclModule.getId()) > 0) {
			throw new ParamException("当前模块下面有用户，无法删除");
		}
		sysAclModuleMapper.deleteByPrimaryKey(aclModuleId);
	}



}
