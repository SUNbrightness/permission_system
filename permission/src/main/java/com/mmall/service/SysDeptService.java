package com.mmall.service;

import com.google.common.base.Preconditions;
import com.mmall.dao.SysDeptMapper;
import com.mmall.dao.SysUserMapper;
import com.mmall.exception.ParamException;
import com.mmall.model.SysDept;
import com.mmall.param.DeptParam;
import com.mmall.util.BeanValidator;
import com.mmall.util.LevelUtil;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * @Auther 19135
 * @Date 2019/1/13 19:22
 * @Description ${end}$
 */
@Service
public class SysDeptService {

	@Resource
	private SysDeptMapper sysDeptMapper;

	@Resource
	private SysUserMapper sysUserMapper;

	public void sava(DeptParam param) {
		BeanValidator.check(param);
		if (checkExist(param.getParentId(), param.getName(), param.getId())) {
			throw new ParamException("同一层级下存在相同名称的部门");
		}
		SysDept dept = new SysDept();
		BeanUtils.copyProperties(param, dept);
		dept.setLevel(LevelUtil.calculateLevel(getLevel(dept.getParentId()), param.getParentId()));
		dept.setOperator("system");// TODO
		dept.setOperateIp("127.0.0.1");
		dept.setOperateTime(new Date());
		sysDeptMapper.insertSelective(dept);

	}

	//判断是同级否存在部门名字重复
	//传id是为了判断分辨不是自己本身
	private boolean checkExist(Integer parentId, String deptName, Integer deptId) {
		return sysDeptMapper.countByNameAndParentId(parentId, deptName, deptId) > 0;
	}

	private String getLevel(Integer deptId) {
		SysDept dept = sysDeptMapper.selectByPrimaryKey(deptId);
		if (dept == null) {
			return null;
		}
		return dept.getLevel();
	}

	public void update(DeptParam param) {
		BeanValidator.check(param);
		if (checkExist(param.getParentId(), param.getName(), param.getId())) {
			throw new ParamException("同一层级下存在相同名称的部门");
		}
		SysDept before = sysDeptMapper.selectByPrimaryKey(param.getId());
		Preconditions.checkNotNull(before, "待更新的部门不存在");

		SysDept after = new SysDept();
		BeanUtils.copyProperties(param, after);

		//更新他的level，变成新的level
		after.setLevel(LevelUtil.calculateLevel(getLevel(param.getParentId()), param.getParentId()));
		after.setOperator("system");// TODO
		after.setOperateIp("127.0.0.1");
		after.setOperateTime(new Date());
		updateWithChild(before, after);

	}

	private void updateWithChild(SysDept before, SysDept after) {
		//更新
		sysDeptMapper.updateByPrimaryKey(after);
		String newLevelPrefix = after.getLevel();
		String oldLevelPrefix = before.getLevel();
		//如果层级不同的话，需要更新该部门下的所有子部门
		if (!newLevelPrefix.equals(oldLevelPrefix)) {
			//查询所有子部门
			List<SysDept> deptList = sysDeptMapper.getChildDeptListByLevel(newLevelPrefix);
			if (CollectionUtils.isNotEmpty(deptList)) {
				for (SysDept dept : deptList) {
					String level = dept.getLevel();
					//这里判断是否是以oldlevel开头的、、感觉判断是多余的。因为这是查询条件
					if (level.indexOf(oldLevelPrefix) == 0) {
						//新的level
						level = newLevelPrefix + level.substring(oldLevelPrefix.length());
						dept.setLevel(level);
					}
				}
				sysDeptMapper.batchUpdateLevel(deptList);
			}
			sysDeptMapper.updateByPrimaryKeySelective(after);
		}
	}

	public void delete(int deptId){
		SysDept dept = sysDeptMapper.selectByPrimaryKey(deptId);
		Preconditions.checkNotNull(dept,"待删除的部门不存在，无法删除");
		if (sysDeptMapper.countByParentId(dept.getParentId())>0){
			throw new ParamException("当前部门下面有子部门，无法删除");
		}
		if (sysUserMapper.countByDeptId(dept.getId())>0){
			throw  new ParamException("当前部门下面有用户，无法删除");
		}
		sysDeptMapper.deleteByPrimaryKey(deptId);
	}


}
