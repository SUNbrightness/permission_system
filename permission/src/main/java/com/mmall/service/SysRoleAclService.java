package com.mmall.service;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.mmall.beans.LogType;
import com.mmall.common.RequestHolder;
import com.mmall.dao.SysLogMapper;
import com.mmall.dao.SysRoleAclMapper;
import com.mmall.exception.ParamException;
import com.mmall.model.SysLogWithBLOBs;
import com.mmall.model.SysRole;
import com.mmall.model.SysRoleAcl;
import com.mmall.param.AclModuleParam;
import com.mmall.util.BeanValidator;
import com.mmall.util.IpUtil;
import com.mmall.util.JsonMapper;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * @Auther 19135
 * @Date 2019/3/24 19:51
 * @Description ${end}$
 */
@Service
public class SysRoleAclService {

	@Resource
	private SysRoleAclMapper sysRoleAclMapper;

	@Resource
	private SysLogMapper sysLogMapper;



	public void changeRoleAcls(Integer roleId, List<Integer> aclIdList){

		//查询该角色拥有的所有权限
		List<Integer> originAclIdList = sysRoleAclMapper.getAclIdListByRoleIdList(Lists.newArrayList(roleId));
		//如果数量没有发生变化
		if (originAclIdList.size() == aclIdList.size()){
			Set<Integer> originAclIdSet = Sets.newHashSet(originAclIdList);
			Set<Integer> aclIdSet = Sets.newHashSet(aclIdList);
			originAclIdSet.removeAll(aclIdSet);
			//如果权限没有发生变化，那么就reutnr
			if (CollectionUtils.isEmpty(originAclIdSet)) {
				return;
			}
		}
		updateRoleAcls(roleId, aclIdList);
		saveRoleAclLog(roleId, originAclIdList, aclIdList);
	}

	@Transactional
	public void updateRoleAcls(int roleId,List<Integer> aclIdList){

		//删除原来的所有该角色的关联关系
		sysRoleAclMapper.deleteByRoleId(roleId);

		if (CollectionUtils.isEmpty(aclIdList)){
			return;
		}

		List<SysRoleAcl> roleAclList = Lists.newArrayList();
		for (Integer aclId:aclIdList){
			SysRoleAcl roleAcl = SysRoleAcl.builder().roleId(roleId).aclId(aclId).operator(RequestHolder.getCurrentUser().getUsername())
					.operateIp(IpUtil.getRemoteIp(RequestHolder.getCurrentRequest())).operateTime(new Date()).build();
			roleAclList.add(roleAcl);
		}
		//保存新的角色关联关系
		sysRoleAclMapper.batchInsert(roleAclList);
	}

	private void saveRoleAclLog(int roleId, List<Integer> before, List<Integer> after) {
		SysLogWithBLOBs sysLog = new SysLogWithBLOBs();
		sysLog.setType(LogType.TYPE_ROLE_ACL);
		sysLog.setTargetId(roleId);
		sysLog.setOldValue(before == null ? "" : JsonMapper.obj2String(before));
		sysLog.setNewValue(after == null ? "" : JsonMapper.obj2String(after));
		sysLog.setOperator(RequestHolder.getCurrentUser().getUsername());
		sysLog.setOperateIp(IpUtil.getRemoteIp(RequestHolder.getCurrentRequest()));
		sysLog.setOperateTime(new Date());
		sysLog.setStatus(1);
		sysLogMapper.insertSelective(sysLog);
	}



}

