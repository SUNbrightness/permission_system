package com.mmall.model;


import lombok.*;
import org.hibernate.validator.constraints.NotBlank;

import java.util.Date;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Getter
@Setter
@EqualsAndHashCode(of = {"id"})
public class SysAcl {
    private Integer id;

    @NotBlank
    private String code;

    private String name;

    private Integer aclModuleId;

    private String url;

    private Integer type;
    private Integer status;

    private Integer seq;

    private String remark;

    private String operator;

    private Date operateTime;

    private String operateIp;


}