package com.mmall.dao;

import com.mmall.beans.PageQuery;
import com.mmall.model.SysUser;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SysUserMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(SysUser record);

    int insertSelective(SysUser record);

    SysUser selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(SysUser record);

    int updateByPrimaryKey(SysUser record);

    //查询该标识符（手机号或者邮箱）是否已经被占用，查找一下
    SysUser findByKeyword(@Param("keyword") String keyword);

    //统计所有该mail注册的数量
    int countByMail(@Param("mail") String mail, @Param("id") Integer id);

    //统计该手机号注册的数量
    int countByTelephone(@Param("telephone") String telephone, @Param("id") Integer id);

    //统计该部门所有的用户id数量
    int countByDeptId(@Param("deptId") int deptId);

    //获取该部门所有用户 并且分页
    List<SysUser> getPageByDeptId(@Param("deptId") int deptId, @Param("page") PageQuery page);

    //根据用户id批量获取用户
    List<SysUser> getByIdList(@Param("idList") List<Integer> idList);

    //获取所有用户
    List<SysUser> getAll();

}