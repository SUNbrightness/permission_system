package com.mmall.dao;

import com.mmall.model.SysRoleUser;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SysRoleUserMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(SysRoleUser record);

    int insertSelective(SysRoleUser record);

    SysRoleUser selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(SysRoleUser record);

    int updateByPrimaryKey(SysRoleUser record);

    //获取该用户拥有的所有角色id
    List<Integer> getRoleIdListByUserId(@Param("userId") int userId);

    //获取该角色拥有的所有用户id
    List<Integer> getUserIdListByRoleId(@Param("roleId") int roleId);

    //删除改角色所有的关联关系
    void deleteByRoleId(@Param("roleId") int roleId);

    //批量创建角色用户关联关系
    void batchInsert(@Param("roleUserList") List<SysRoleUser> roleUserList);

    //批量查询角色用户关联关系
    List<Integer> getUserIdListByRoleIdList(@Param("roleIdList") List<Integer> roleIdList);
}