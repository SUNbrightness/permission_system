package com.mmall.dao;

import com.mmall.model.SysAclModule;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SysAclModuleMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(SysAclModule record);

    int insertSelective(SysAclModule record);

    SysAclModule selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(SysAclModule record);

    int updateByPrimaryKey(SysAclModule record);

    //根据名字和父级id获取model数量
    int countByNameAndParentId(@Param("parentId") Integer parentId, @Param("name") String name, @Param("id") Integer id);

    //获取该层级下的所有model
    List<SysAclModule> getChildAclModuleListByLevel(@Param("level") String level);

    //批量更新层级
    void batchUpdateLevel(@Param("sysAclModuleList") List<SysAclModule> sysAclModuleList);

    //获取所有的model
    List<SysAclModule> getAllAclModule();

    //计算该model有多少个直接儿子
    int countByParentId(@Param("aclModuleId") int aclModuleId);
}