package com.mmall.dao;

import com.mmall.beans.PageQuery;
import com.mmall.dto.SearchLogDto;
import com.mmall.model.SysLog;
import com.mmall.model.SysLogWithBLOBs;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SysLogMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(SysLogWithBLOBs record);

    int insertSelective(SysLogWithBLOBs record);

    SysLogWithBLOBs selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(SysLogWithBLOBs record);

    int updateByPrimaryKeyWithBLOBs(SysLogWithBLOBs record);

    int updateByPrimaryKey(SysLog record);

    //根据查询条件进行查询
    int countBySearchDto(@Param("dto") SearchLogDto dto);

    //根据查询条件，以以及分页条件进行查询
    List<SysLogWithBLOBs> getPageListBySearchDto(@Param("dto") SearchLogDto dto, @Param("page") PageQuery page);
}