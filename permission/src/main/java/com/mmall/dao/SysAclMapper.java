package com.mmall.dao;

import com.mmall.beans.PageQuery;
import com.mmall.model.SysAcl;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SysAclMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(SysAcl record);

    int insertSelective(SysAcl record);

    SysAcl selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(SysAcl record);

    int updateByPrimaryKey(SysAcl record);

    //获取该权限模块的权限数量
    int countByAclModuleId(@Param("aclModuleId") int aclModuleId);

    //根据模块id，模块名称，模块id获取权限数量
    int countByNameAndAclModuleId(@Param("aclModuleId") int aclModuleId, @Param("name") String name, @Param("id") Integer id);

    //获取所有权限
    List<SysAcl> getAll();

    //根据权限id集合获取权限id
    List<SysAcl> getByIdList(@Param("idList") List<Integer> idList);

    //获取可以进入该url的权限
    List<SysAcl> getByUrl(@Param("url") String url);

    //获取该模块所有权限，并且分页
    List<SysAcl> getPageByAclModuleId(@Param("aclModuleId") int aclModuleId, @Param("page") PageQuery page);

}