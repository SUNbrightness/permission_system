package com.mmall.dao;

import com.mmall.model.SysRoleAcl;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SysRoleAclMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(SysRoleAcl record);

    int insertSelective(SysRoleAcl record);

    SysRoleAcl selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(SysRoleAcl record);

    int updateByPrimaryKey(SysRoleAcl record);

    //查询角色集合所拥有的所有权限id
    List<Integer> getAclIdListByRoleIdList(@Param("roleIdList") List<Integer> roleIdList);

    //根据删除所有与本角色关联的关联关系
    void deleteByRoleId(@Param("roleId") int roleId);

    //批量创建多条角色权限关联
    void batchInsert(@Param("roleAclList") List<SysRoleAcl> roleAclList);

    //获取拥有该权限的所有角色id
    List<Integer> getRoleIdListByAclId(@Param("aclId") int aclId);
}