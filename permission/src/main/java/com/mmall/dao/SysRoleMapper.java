package com.mmall.dao;

import com.mmall.model.SysRole;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SysRoleMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(SysRole record);

    int insertSelective(SysRole record);

    SysRole selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(SysRole record);

    int updateByPrimaryKey(SysRole record);

    //获取所有角色
    List<SysRole> getAll();

    //统计该角色数量
    int countByName(@Param("name") String name, @Param("id") Integer id);

    //获取根据角色id列表获取角色
    List<SysRole> getByIdList(@Param("idList") List<Integer> idList);
}