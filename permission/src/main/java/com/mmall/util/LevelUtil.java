package com.mmall.util;


import org.apache.commons.lang3.StringUtils;

/**
 * @Auther 19135
 * @Date 2019/1/1 16:52
 * @Description level专用的工具类
 */
public class LevelUtil {

	//分割符号
	public final static  String SEPARATOR = ".";

	//根节点
	public final static  String ROOT = "0";

	//0
	//0.1
	//0.1.2
	//每一个数字代表一个id
	//这样的好处可以顺着找到上一层
	public static String calculateLevel(String parentLevel,int parentId){
		if (StringUtils.isBlank(parentLevel)){
			return ROOT;
		}
		return StringUtils.join(parentLevel,SEPARATOR,parentId);
	}

}
