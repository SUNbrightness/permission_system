package com.mmall.beans;

import com.google.common.collect.Lists;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

/**
 * @Auther 19135
 * @Date 2019/3/21 20:30
 * @Description ${返回分页时的bean}$
 */
@Getter
@Setter
@ToString
@Builder
public class PageResult<T> {

	private List<T> data = Lists.newArrayList();

	private int total = 0;

}
