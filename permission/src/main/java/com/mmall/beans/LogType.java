package com.mmall.beans;

/**
 * @Auther 19135
 * @Date 2019/3/21 20:30
 * @Description ${end}$
 */
public interface LogType {

	int TYPE_DEPT = 1;

	int TYPE_USER = 2;

	int TYPE_ACL_MODULE = 3;

	int TYPE_ACL = 4;

	int TYPE_ROLE = 5;

	int TYPE_ROLE_ACL = 6;

	int TYPE_ROLE_USER = 7;
}
