package com.mmall.beans;

import lombok.Getter;

/**
 * @Auther 19135
 * @Date 2019/3/21 20:29
 * @Description ${end}$
 */
@Getter
public enum  CacheKeyConstants {

	SYSTEM_ACLS,

	USER_ACLS;
}
