package com.mmall.demo2.service;

import com.mmall.demo2.mapper.UserMapper;
import com.mmall.demo2.model.User;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @Auther 19135
 * @Date 2018/12/9 21:04
 * @Description $end$
 */
@Service
public class UserServiceImpl implements UserService {

    @Resource
    private UserMapper userMapper;

    @Override
    public User findByUsername(String name) {
        return userMapper.findByUsername(name);
    }
}
