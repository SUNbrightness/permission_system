package com.mmall.demo2.service;

import com.mmall.demo2.model.User;

/**
 * @Auther 19135
 * @Date 2018/12/9 21:03
 * @Description $end$
 */
public interface UserService {

    User findByUsername(String name);



}
