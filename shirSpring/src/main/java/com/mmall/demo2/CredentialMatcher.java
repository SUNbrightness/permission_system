package com.mmall.demo2;

import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authc.credential.SimpleCredentialsMatcher;

/**
 * @Auther guo
 * @Date 2018/12/15 11:20
 * @Description 重写的认证登录后的密码效验方法
 */
public class CredentialMatcher extends SimpleCredentialsMatcher {


    //最基本的密码效验规则重写
    @Override
    public boolean doCredentialsMatch(AuthenticationToken token, AuthenticationInfo info) {

        UsernamePasswordToken usernamePasswordToken = (UsernamePasswordToken) token;
        String password = new String(usernamePasswordToken.getPassword());
        String dbPassword = String.valueOf(info.getCredentials());
        return this.equals(password,dbPassword);
    }
}
