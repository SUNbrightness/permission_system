package com.mmall.demo2.mapper;

import com.mmall.demo2.model.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @Auther guo
 * @Date 2018/12/9 21:02
 * @Description $end$
 */

public interface UserMapper {

    User findByUsername(@Param("username") String username);

}
